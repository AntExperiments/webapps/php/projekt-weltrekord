<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        //TODO: check
        $pwd = hash("sha512", $_POST["userUser"] . $_POST["userPasswd"]);
        $content = '<?php
            $pswdHash = \'' . $pwd . '\';
            $servername = \'' . $_POST["dbHost"] . '\';
            $username = \'' . $_POST["dbUser"] . '\';
            $password = \'' . $_POST["dbPasswd"] . '\';
            $dbname = \'' . $_POST["dbName"] . '\';
        ?>';
        file_put_contents('config.php', $content);
        
        include 'php/import_tables.php';

        if (isset($_POST['addSampleUsers'])) {
            include 'php/import_names.php';
        }
        
        if (isset($_POST['addSampleProsts'])) {
            include 'php/import_prosts.php';
        }

        header("Location: .");
    }
?>
<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Projekt Weltrekord - Installieren</title>
    
    <script src="js/utility.js" defer></script>

    <?php include 'php/layout.php' ?>
</head>

<body>
    <!-- Navigation Bar -->
    <?php getNav() ?>

    <div class="container">
        <h1>Installieren</h1>
        <div class="row">
            <form class="col s12" action="install.php" method="POST">
                <div class="row">
                    <div class="input-field col s6">
                        <input id="dbHost" name="dbHost" type="text" class="validate" value="127.0.0.1" require>
                        <label for="dbHost">Datenbank-Host</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="dbName" name="dbName" type="text" class="validate" placeholder="weltrekord" require>
                        <label for="dbName">Datenbank</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input id="dbUser" name="dbUser" type="text" class="validate" require>
                        <label for="dbUser">Datenbank-Nutzer</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="dbPasswd" name="dbPasswd" type="password" class="validate" require>
                        <label for="dbPasswd">Datenbank-Password</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <input id="userUser" name="userUser" type="text" class="validate" require>
                        <label for="userUser">Webui-Nutzer</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="userPasswd" name="userPasswd" type="password" class="validate" require>
                        <label for="userPasswd">Webui-Password</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">
                        <p>
                            <label>
                                <input type="checkbox" name="addSampleUsers" />
                                <span>Beispiel-Nutzer hinzufügen</span>
                            </label>
                        </p>
                    </div>
                    <div class="input-field col s4">
                        <p>
                            <label>
                                <input type="checkbox" name="addSampleProsts" />
                                <span>Beispiel-Prosts hinzufügen</span>
                            </label>
                        </p>
                    </div>
                    <button class="btn waves-effect waves-light" type="submit" name="action" style="float:right">Installieren
                        <i class="material-icons right">file_download</i>
                    </button>
                </div>
            </form>
        </div>
    </div>
    
    <!-- Logout Modal -->
    <?php getModal() ?>

    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>