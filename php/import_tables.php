<?php
    include "_sql-login.php";

    // run SQL statement that creates the Table 'personen'
    $conn->query("CREATE TABLE `personen` (
        `ID` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
        `first_name` varchar(48) NOT NULL,
        `last_name` varchar(48) NOT NULL,
        `dateOfCreation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");

    // run SQL statement that creates the Table 'prosts'
    $conn->query("CREATE TABLE `prosts` (
        `id1` int(11) NOT NULL,
        `id2` int(11) NOT NULL,
        `dateOfCreation` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;");
    
    $conn->close();
?>