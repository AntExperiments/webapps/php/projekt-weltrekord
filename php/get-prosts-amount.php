<?php
    include "_sql-login.php";

    // definie SQL statement
    $sql= "SELECT DISTINCT CASE WHEN id1<id2 THEN id1 ELSE id2 END AS FirstColumn, CASE WHEN id1<id2 THEN id2 ELSE id1 END AS SecondColumn From prosts GROUP BY id1, id2;";
    
    // execute SQL statement and save it in $result
    $result = $conn->query($sql);

    // create counter-variable
    $output = 0;

    // count everything together
    if ($result->num_rows > 0)
      while($row = $result->fetch_assoc())
        $output++;

    // output result
    echo $output;

    $conn->close();
?>