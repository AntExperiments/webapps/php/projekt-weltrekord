<?php
    include "_sql-login.php";

    // open file handler
    $handle = fopen("php/names.txt", "r");
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            // convert the string of a row and seperate by a space
            // (this assums that the sample-data consists of a first-name and a last-name, which are seperated by a space)
            $line = explode(" ", $line);
            // fill variables and make sure to strip all tags (althoug there won't be any because this is not an user-input but just to be sure)
            $first_name = strip_tags($line[0]);
            $last_name = strip_tags($line[1]);

            // define SQL statement
            $sql = "INSERT INTO personen (first_name, last_name) VALUES ('$first_name', '$last_name')";

            // execute SQL statement
            $conn->query($sql);
        }

        // close file handler
        fclose($handle);
    } else
        // display Error message
        echo "Error! Couldn't open the file";

    $conn->close();
?> 