<?php
    function getModal() {
        echo "<div id='modal1' class='modal'>
            <div class='modal-content'>
                <h4>Möchten Sie sich wirklich ausloggen?</h4>
                <p>Falls Sie diese WebApp wieder verwenden müssen, müssen Sie sich wieder einloggen.</p>
            </div>
            <div class='modal-footer'>
                <a href='#' class='modal-close waves-effect waves-green btn-flat'>Cancel</a>
                <a href='php/remove-login.php' class='modal-close waves-effect waves-green btn'>Logout</a>
            </div>
        </div>'";
    }

    function getNav($activeOne = "") {
        echo "<nav class='blue lighten-2'>
            <div class='nav-wrapper'>
                <a href='.' class='brand-logo left' style='margin-left: 15px'><i class='material-icons'>cloud</i><span class='hide-on-small-only'>Projekt Weltrekord</span></a>
                <ul class='right'>
                    <li" . (($activeOne == "stats") ? " class='active'" : "") . ">
                        <a href='stats.php'><i class='material-icons'>insert_chart</i></a>
                    </li>
                    <li" . (($activeOne == "index") ? " class='active'" : "") . ">
                        <a href='index.php'><i class='material-icons'>person</i></a>
                    </li>
                    <li" . (($activeOne == "register") ? " class='active'" : "") . ">
                        <a href='register.php'><i class='material-icons'>add</i></a>
                    </li>
                    <li>
                        <a class='modal-trigger' href='#modal1'><i class='material-icons'>logout</i></a>
                    </li>
                </ul>
            </div>
        </nav>";
    }
?>