<?php
    include "_sql-login.php";

    // define SQL statement
    $sql= "SELECT first_name, last_name, count(*) AS NumberOfOccurances From prosts LEFT JOIN personen ON id1=ID Group By first_name, last_name ORDER BY NumberOfOccurances DESC, first_name LIMIT 5";

    // execute SQL statement
    $result = $conn->query($sql);

    // run through every result
    if ($result->num_rows > 0) {
      // define $i wich represents the rank on the podium
      $i = 1;
      while($row = $result->fetch_assoc()) {
        // output the result as a string that's formated like a CSV but seperated with a Paragraph-Sign (§)
        echo "$i. " . $row["first_name"]. " " . $row["last_name"]. " (" . $row["NumberOfOccurances"]. " Prosts)§";
        // increase the rank on the podium
        $i++;
      }
    } else
      // output "Error-Message" if no data has been entered yet
      echo "No Data yet";

    $conn->close();
?>