<?php
    include "_sql-login.php";

    //TODO: make save

    $error = false;

    // get people
    $resultPeople = $conn->query("SELECT ID FROM personen;");
    $people = [];
    if ($resultPeople->num_rows > 0)
        while($row = $resultPeople->fetch_assoc())
            array_push($people, $row['ID']);

    // variables
    $prost = strip_tags($_POST['proster']);
    $prostsToBeAdded = array();

    for ($i=1; $i <= 12; $i++)
        ($_POST["proster$i"] != "") ? array_push($prostsToBeAdded, $_POST["proster$i"]) : "";

    // do SQL
    foreach ($prostsToBeAdded as &$value) {
        $value = strip_tags($value);
        $sql = "INSERT INTO prosts (id1, id2) VALUES ('$prost', $value)";

        if (in_array($value, $people) == false || $prost == $value)
            $error = true;
        else
            if ($conn->query($sql) === TRUE)
                echo "New record created successfully";
            else
                $error = true;
        
    }

    // redirect and give correct error or success message
    if ($error == false)
        header('Location: ../register.php?success=true');
    else
        header('Location: ../register.php?error=true');

    $conn->close();
?> 