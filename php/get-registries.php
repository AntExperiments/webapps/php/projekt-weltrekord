<?php
    include "_sql-login.php";

    // Define output as an array
    $output = array();

    for ($i=0; $i < 7; $i++) { 
        // define date from which to get the date (formatted as ISO)
        $dateFrom = date("Y-m-d") . " " . date("H", time() + 1800 - ((($i + 1) / 2) * 3600)) . ":" . ((date("i", time() + 1800 - ((($i + 1) % 2) * 1800) + 0) >= 30) ? "30" : "00");
        // define date from which to get the date (formatted as ISO)
        $dateTo = date("Y-m-d") . " " . date("H", time() + 1800 - (($i / 2) * 3600)) . ":" . ((date("i", time() + 1800 - (($i % 2) * 1800) + 0) >= 30) ? "30" : "00");

        // define SQL statement
        $sql= "SELECT count(*) AS NumberOfOccurances From personen Where dateOfCreation Between '$dateFrom' And '$dateTo' ORDER By dateOfCreation";
        $result = $conn->query($sql);

        // push the results to the beginning of the output
        while($row = $result->fetch_assoc())
            array_unshift($output, $row['NumberOfOccurances']);
    }

    // output the array
    foreach ($output as $key => $value) {
        echo $value;
        // if it's the last value, don't add a comma (,)
        echo ($key != sizeof($output) - 1) ? "," : "" ;
    }

    $conn->close();
?>