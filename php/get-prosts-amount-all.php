<?php
    include "_sql-login.php";

    // execute SQL statement and save it in $result
    $result = $conn->query("SELECT Count(*) as amount From prosts;");

    // create counter-variable
    $output = 0;

    // count everything together
    if ($result->num_rows > 0)
        while($row = $result->fetch_assoc())
          $output += $row["amount"];

    // output result
    echo $output;

    $conn->close();
?>