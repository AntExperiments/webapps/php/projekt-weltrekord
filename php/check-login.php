<?php
    include_once 'config.php';

    $wrongPasswd = false;

    // check if login is not set
    if (!isset($_COOKIE["email"]) || !isset($_COOKIE["email"]))
        $wrongPasswd = true;

    // check if login is incorrect
    else if (hash("sha512", $_COOKIE["email"] . $_COOKIE["password"]) != $pswdHash)
        $wrongPasswd = true;    
    
    // echo $_COOKIE["email"];
    // echo $_COOKIE["password"];
    // echo hash("sha512", $_COOKIE["email"] . $_COOKIE["password"]);

    // stop loading web-page and echo the "Login-Page"
    if ($wrongPasswd == true) {
        echo "<div class='container' style='margin-top: 30px'><div class='row'><h1>Please Login</h1></div>
            <div class='row'><form class='col s12' action='php/set-login.php' method='POST'>
            <div class='row'><div class='input-field col s12'>
                <input id='email' name='email' type='text' class='validate'>
                <label for='email'>Username</label></div>
            </div><div class='row'><div class='input-field col s12'>
                <input id='password' name='password' type='password' class='validate'>
                <label for='password'>Password</label></div>
            </div><div class='row'>
                <button class='btn waves-effect waves-light' type='submit' name='action' style='float:right'>Login
                <i class='material-icons right'>send</i>
                </button></div>
          </form></div></div><script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js'></script></body></html>";
        exit();
    }
?>