<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Projekt Weltrekord - Registrieren</title>

    <script src="js/utility.js" defer></script>

    <?php include 'php/layout.php' ?>
</head>

<body>
    <!-- Navigation Bar -->
    <?php getNav("register") ?>

    <div class="container">
        <?php include_once("php/check-login.php"); ?>

        <h1>Prosts Registrieren</h1>
        <div class="row">
            <form class="col s12" action="php/add-prost.php" method="POST">
                <div class="row">
                    <div class="input-field col s12 m5">
                        <select name="proster" required onchange="clearProsts()">
                            <option value="" disabled selected>Person Auswählen</option>
                            <?php
                                include "php/_sql-login.php";

                                $sql = "SELECT ID, first_name, last_name FROM personen";
                                $result = $conn->query($sql);

                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_assoc()) {
                                        echo "<option value='" . strip_tags($row["ID"]) ."'>" . strip_tags($row["ID"]) . " " . strip_tags($row["first_name"]) . " " . strip_tags($row["last_name"]) . "</option>";
                                    }
                                } else {
                                    echo "0 results";
                                }

                                $conn->close();
                            ?> 
                        </select>
                        <label>Materialize Select</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6 m3">
                        <input id="proster1" name="proster1" type="number" class="validate clearOnChange">
                        <label for="proster1">Proster</label>
                    </div>
                    <div class="input-field col s6 m3">
                        <input id="proster2" name="proster2" type="number" class="validate clearOnChange">
                        <label for="proster2">Proster</label>
                    </div>
                    <div class="input-field col s6 m3">
                        <input id="proster3" name="proster3" type="number" class="validate clearOnChange">
                        <label for="proster3">Proster</label>
                    </div>
                    <div class="input-field col s6 m3">
                        <input id="proster4" name="proster4" type="number" class="validate clearOnChange">
                        <label for="proster4">Proster</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6 m3">
                        <input id="proster5" name="proster5" type="number" class="validate clearOnChange">
                        <label for="proster5">Proster</label>
                    </div>
                    <div class="input-field col s6 m3">
                        <input id="proster6" name="proster6" type="number" class="validate clearOnChange">
                        <label for="proster6">Proster</label>
                    </div>
                    <div class="input-field col s6 m3">
                        <input id="proster7" name="proster7" type="number" class="validate clearOnChange">
                        <label for="proster7">Proster</label>
                    </div>
                    <div class="input-field col s6 m3">
                        <input id="proster8" name="proster8" type="number" class="validate clearOnChange">
                        <label for="proster8">Proster</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6 m3">
                        <input id="proster9" name="proster9" type="number" class="validate clearOnChange">
                        <label for="proster9">Proster</label>
                    </div>
                    <div class="input-field col s6 m3">
                        <input id="proster10" name="proster10" type="number" class="validate clearOnChange">
                        <label for="proster10">Proster</label>
                    </div>
                    <div class="input-field col s6 m3">
                        <input id="proster11" name="proster11" type="number" class="validate clearOnChange">
                        <label for="proster11">Proster</label>
                    </div>
                    <div class="input-field col s6 m3">
                        <input id="proster12" name="proster12" type="number" class="validate clearOnChange">
                        <label for="proster12">Proster</label>
                    </div>
                </div>
                <div class="row">
                    <button class="btn waves-effect waves-light" type="submit" name="action" style="float:right">Prosts registrieren
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
        </div>
    </div>
    
    <!-- Logout Modal -->
    <?php getModal() ?>

    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('select');
            var instances = M.FormSelect.init(elems);
        });
    </script>
</body>
</html>
        